#include "../CopyCaloCalibrationHitContainer.h"
#include "../CopyJetTruthInfo.h"
#include "../CopyMcEventCollection.h"
#include "../CopyTimings.h"
#include "../CopyTrackRecordCollection.h"

#include "../BSFilter.h"
#include "../ByteStreamMultipleOutputStreamCopyTool.h"

DECLARE_COMPONENT( CopyCaloCalibrationHitContainer )
DECLARE_COMPONENT( CopyJetTruthInfo )
DECLARE_COMPONENT( CopyMcEventCollection )
DECLARE_COMPONENT( CopyTimings )
DECLARE_COMPONENT( CopyTrackRecordCollection )

DECLARE_COMPONENT( BSFilter )
DECLARE_COMPONENT( ByteStreamMultipleOutputStreamCopyTool )
